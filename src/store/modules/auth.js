import { AUTH_LOGIN, AUTH_LOGOUT } from '../mutation-types'
import rounter from '../../router'

export default {
  namespaced: true,

  state: () => ({
    user: null
  }),
  mutations: {
    [AUTH_LOGIN] (state, payload) {
      state.user = payload
    },
    [AUTH_LOGOUT] (state) {
      state.user = null
    }
  },
  actions: {
    login ({ commit }, payload) {
      console.log(payload)
      // if correct
      const user = { name: 'Saksarak', email: 'saklaw33@gmail.com' }
      rounter.push('/')
      commit(AUTH_LOGIN, user)
    },
    logout ({ commit }) {
      commit(AUTH_LOGOUT)
    }
  },
  getters: {
    isLogin (state, getters) {
      return state.user != null
    }
  }
}
